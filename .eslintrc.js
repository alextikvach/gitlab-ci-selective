module.exports = {
  extends: 'airbnb-base',
  rules: {
    'no-restricted-syntax': 'off',
    'class-methods-use-this': 'off',
    'object-curly-spacing': ['error', 'never'],
    'arrow-parens': ['error', 'always'],
    'comma-dangle': ['error', {
      'functions': 'ignore',
      'objects': 'always-multiline',
      'arrays': 'always-multiline',
    }],
    'no-use-before-define': ['error', {'functions': false}],
    'arrow-body-style': 'off',
    'no-underscore-dangle': 'off',
    'no-console': 'off',
    'import/prefer-default-export': 'off'
  }
}
