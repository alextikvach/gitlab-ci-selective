const _ = require('lodash');
const debug = require('debug')('merge-request-retriver');

class GitlabMergeRequest {
  constructor({api, sha} = {}) {
    if (!sha) {
      throw new Error('"sha" is missmatch');
    }

    this._api = api;
    this._sha = sha;
    this._iid = null;
  }

  static isMr({api, sha}) {
    debug('Checking: is sha mr?');

    const gmr = new GitlabMergeRequest({api, sha});

    return gmr._findIidOfMr()
      .then(() => true)
      .catch((err) => {
        debug(err.message);
        return false;
      });
  }

  addComment(body) {
    return this._api
      .post(`merge_requests/${this._iid}/notes`, {body})
      .then((res) => {
        debug(`Send message response: ${JSON.stringify(res)}`);
      });
  }

  affectedFiles() {
    return this._findIidOfMr()
      .then((iid) => {
        this._iid = iid;
        return this._extractAffectedFiles();
      });
  }

  _findIidOfMr() {
    return this._api
      .get('merge_requests', {state: 'opened'})
      .then((list) => {
        debug(`Recived list of oepened MRs:\n${JSON.stringify(list)}`);

        if (_.isEmpty(list)) {
          throw new Error('Unable to find opened merge requests');
        }

        const mr = _.find(list, {sha: this._sha});

        if (!mr) {
          throw new Error(
            `Unable to find opened MR by sha=${this._sha}.\nAvailable: ${_.map(list, 'sha')}`
          );
        }

        return mr.iid;
      });
  }

  _extractAffectedFiles() {
    return this._api.get(`merge_requests/${this._iid}/changes`)
      .then((mr) => {
        debug(`Recived MR changes:\n${JSON.stringify(mr)}`);

        if (!mr || !mr.changes) {
          throw new Error(`No changes for mr iid=${this._iid}`);
        }

        const affectedFiles = _(mr.changes)
          .map((diff) => [diff.old_path, diff.new_path])
          .flatten()
          .uniq()
          .value();

        debug(`Affected files: ${affectedFiles}`);

        return affectedFiles;
      });
  }
}

module.exports = GitlabMergeRequest;
