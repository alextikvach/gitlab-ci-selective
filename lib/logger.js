exports.log = console.log.bind(console);
exports.warn = console.warn.bind(console);
exports.error = console.error.bind(console);
exports.time = console.time.bind(console);
exports.timeEnd = console.timeEnd.bind(console);
