const yaml = require('js-yaml');
const fs = require('fs');

class Config {
  constructor(configFilePath) {
    this._configFilePath = configFilePath;
    this._config = this._readConfig();
  }

  _readConfig() {
    const configContent = fs.readFileSync(this._configFilePath, 'utf8');

    try {
      return yaml.safeLoad(configContent);
    } catch (e) {
      throw new Error(`Unable to load YAML file with content: ${configContent}`);
    }
  }

  getStageDeclaration(stageName) {
    const declaration = this._config.stages[stageName];

    if (!declaration) {
      throw new Error(
        `There is no "${stageName}" stage in config file. Available stages: ${Object.keys(this._config)}`
      );
    }

    return declaration;
  }

  getOption(key) {
    const settings = this._config.settings || {};
    return settings[key];
  }
}

module.exports = Config;
