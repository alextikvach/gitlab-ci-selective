#!/usr/bin/env node

const _ = require('lodash');
const executeStage = require('../lib/executor');
const utils = require('../lib/utils');
const P = require('q');
const Config = require('../lib/config');
const MrStage = require('../lib/stage/mr');
const BranchStage = require('../lib/stage/branch');
const GitlabMergeRequest = require('../lib/gitlab-merge-request');
const GitlabApi = require('../lib/gitlab-api');

exports.run = ({configPath, stageName, projectId, token, sha}) => {
  const config = new Config(configPath);
  const endpoint = config.getOption('endpoint');
  const api = new GitlabApi({endpoint, token, projectId});

  return utils
    .determineDestType({api, sha})
    .then((destType) => {
      console.log(`Running for "${destType}"`);

      return {
        [utils.destTypes.branch]: _branchFlow,
        [utils.destTypes.mr]: _mrFlow,
      }[destType]();
    });

  function _branchFlow() {
    const stage = new BranchStage({config, stageName});

    return executeStage(stage);
  }

  function _mrFlow() {
    const gmr = new GitlabMergeRequest({api, sha});

    return gmr
      .affectedFiles()
      .then((affectedFilesList) => {
        const stage = new MrStage({config, stageName, affectedFilesList});

        return P.all([
          _addComment(stage),
          executeStage(stage),
        ]);
      });

    function _addComment(stage) {
      let msg = [
        '### selective-ci report',
        `- **Stage**: \`${stageName}\``,
        `- **Commit**: \`${sha}\``,
      ];

      msg = _.isEmpty(stage.patterns)
        ? msg.concat('- **Status**: `skip (no changes to run)`')
        : msg.concat('- **Status**: `running`');

      return gmr.addComment(msg.join('\n'));
    }
  }
};
