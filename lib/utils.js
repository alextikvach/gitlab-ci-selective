const _ = require('lodash');
const GitlabMergeRequest = require('../lib/gitlab-merge-request');

exports.replaceEnvPlaceholder = (tpl) => {
  try {
    return _.template(tpl)(process.env);
  } catch (ReferenceError) {
    throw new Error(
      `Unable to resolve template "${tpl}".\nENV: ${JSON.stringify(process.env, 0, 2)}`
    );
  }
};

const destTypes = {
  mr: 'mr',
  branch: 'branch',
};

exports.destTypes = destTypes;

exports.determineDestType = ({api, sha}) => {
  return GitlabMergeRequest
    .isMr({api, sha})
    .then((isMr) => {
      return {
        true: destTypes.mr,
        false: destTypes.branch,
      }[isMr];
    });
};
