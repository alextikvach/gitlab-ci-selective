const minimatch = require('minimatch');
const _ = require('lodash');

class Stage {
  constructor({stageName, declaration, affectedFileList = []}) {
    this._name = stageName;
    this._declaration = declaration;
    this._affectedFileList = affectedFileList;
  }

  get name() {
    return this._name;
  }

  get patterns() {
    const patterns = Object.keys(this._declaration);
    const _isMatch = (pattern) => minimatch
      .match(this._affectedFileList, pattern, {matchBase: true})
      .length;

    return patterns
      .filter(_isMatch)
      .map((pattern) => ({
        pattern,
        commands: this._commandsByPattern(pattern),
        options: this._optionsByPattern(pattern),
      }));
  }

  _commandsByPattern(pattern) {
    const commands = this._declaration[pattern].commands;

    if (_.isEmpty(commands)) {
      throw new Error(`There is no commands for "${pattern}" pattern.`);
    }

    return commands;
  }

  _optionsByPattern(pattern) {
    return this._declaration[pattern].options || {};
  }
}

module.exports = Stage;
