const P = require('q');
const cp = require('child_process');
const logger = require('./logger');
const _ = require('lodash');
const utils = require('./utils');
const debug = require('debug')('executor');

module.exports = function executeStage(stage) {
  return stage.patterns
    .map((commandsSet) => () => {
      debug(`StageSet: ${JSON.stringify(commandsSet)}`);
      logger.log(`<${stage.name}:${commandsSet.pattern}>`);

      return runCommands(commandsSet);
    })
    .reduce(P.when, P.resolve());
};

function runCommands(commandsSet) {
  const opts = {
    shell: true,
    env: getCustomEnv(commandsSet.options.env || {}),
  };

  if (commandsSet.options.cwd) {
    opts.cwd = commandsSet.options.cwd;
  }

  const shell = cp.spawn('bash', opts);

  shell.stdout.on('data', (res) => {
    console.log(`[STDOUT]:\n${bufToOut(res)}`);
  });

  shell.stderr.on('data', (res) => {
    console.log(`[STDERR]:\n${bufToOut(res)}`);
  });

  shell.on('error', (err) => {
    logger.log(`[ERR]: ${err.stack}`);
  });

  for (const command of commandsSet.commands) {
    shell.stdin.write(`${command}\n`);
  }

  shell.stdin.end();

  return new P.Promise((resolve, reject) => {
    shell.on('close', (code) => {
      logger.log('CODE', code);
      if (code) {
        reject(new Error(
          `Last command have finished with non-zero code - ${code}`
        ));
      }
      resolve();
    });
  });
}

function bufToOut(buf) {
  return buf
    .toString()
    .replace(/\s$/, '');
}

function getCustomEnv(env) {
  debug('ENV', env);

  const customEnv = _.mapValues(env || {}, utils.replaceEnvPlaceholder);

  return Object.assign(process.env, customEnv);
}
