const path = require('path');
const request = require('request-promise');
const debug = require('debug')('gitlab-api');

const methods = {
  GET: 'GET',
  POST: 'POST',
};

module.exports = class GitlabApi {
  constructor({endpoint = 'https://gitlab.com/api/v4/', token, projectId} = {}) {
    if (!token) {
      throw new Error('"token" is missmatch');
    }

    if (!projectId) {
      throw new Error('"projectId" is missmatch');
    }

    this._token = token;
    this._endpoint = endpoint;
    this._projectId = projectId;
  }

  get(operationPath, params) {
    return this._invoke({method: methods.GET, operationPath, params});
  }

  post(operationPath, params) {
    return this._invoke({method: methods.POST, operationPath, params});
  }

  _invoke({method, operationPath, params = {}}) {
    const opts = {
      method,
      baseUrl: this._endpoint,
      uri: path.join('/projects', this._projectId, operationPath || ''),
      qs: Object.assign({
        private_token: this._token,
      }, params),
      json: true,
    };

    debug(`Triyng to request with opts: ${JSON.stringify(opts)}`);

    return request(opts);
  }
};
