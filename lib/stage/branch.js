const BaseStage = require('./base');

module.exports = class BranchStage extends BaseStage {
  get patterns() {
    return this._patterns.get();
  }
};
