const Patterns = require('./patterns');

module.exports = class BaseStage {
  constructor({config, stageName}) {
    if (!config) {
      throw new Error('"config" is missmatch');
    }

    if (!stageName) {
      throw new Error('"stageName" is missmatch');
    }

    this._stageName = stageName;
    this._config = config;
    this._patterns = new Patterns(config.getStageDeclaration(stageName));
  }

  get patterns() {
    throw new Error('Not implemented');
  }

  get name() {
    return this._stageName;
  }
};
