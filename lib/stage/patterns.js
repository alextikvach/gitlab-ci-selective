const _ = require('lodash');

module.exports = class Patterns {
  constructor(declaration) {
    this._declaration = declaration;
  }

  _patterns() {
    return Object.keys(this._declaration);
  }

  get() {
    return this._patterns()
      .map((pattern) => ({
        pattern,
        commands: this._commandsByPattern(pattern),
        options: this._optionsByPattern(pattern),
      }));
  }

  _commandsByPattern(pattern) {
    const commands = this._declaration[pattern].commands;

    if (_.isEmpty(commands)) {
      throw new Error(`There is no commands for "${pattern}" pattern.`);
    }

    return commands;
  }

  _optionsByPattern(pattern) {
    return this._declaration[pattern].options || {};
  }
};
