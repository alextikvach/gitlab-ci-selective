const BaseStage = require('./base');
const minimatch = require('minimatch');

module.exports = class BranchStage extends BaseStage {
  constructor({config, stageName, affectedFilesList = []}) {
    super({config, stageName});

    this._affectedFilesList = affectedFilesList;
  }

  get patterns() {
    return this._patterns
      .get()
      .filter((patternSet) => this._matchPattern(patternSet));
  }

  _matchPattern(patternSet) {
    return minimatch
      .match(this._affectedFilesList, patternSet.pattern, {matchBase: true})
      .length;
  }
};
