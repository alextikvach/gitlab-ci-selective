#!/usr/bin/env node

const program = require('commander');
const gitlabCiSelective = require('../lib');

const defaultConfigPath = './.gitlab-ci-selective.yml';

program
  .version(require('../package.json').version)
  .option('--stage [name]', 'Stage name to run')
  .option('--token [hash]', 'Gitlab accesstoken')
  .option('--project [id]', 'Gitlab projectId')
  .option('--sha [hash]', 'Gitlab Merge Request sha')
  .option('--configPath [path]', `Path to config - ${defaultConfigPath}`, defaultConfigPath)
  .parse(process.argv);

['stage', 'configPath', 'token', 'project', 'sha'].forEach((requireOpt) => {
  if (!program[requireOpt]) {
    throw new Error(`Missing required option "${requireOpt}"`);
  }
});

gitlabCiSelective
  .run({
    configPath: program.configPath,
    stageName: program.stage,
    projectId: program.project,
    token: program.token,
    sha: program.sha,
  })
  .done();
