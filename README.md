Tool help to run custom shell commands depends on changes in Merge Request.

Install tool:
```sh
npm i gitlab-ci-selective
```

Write config:
```yaml
testing:
  module1/**:
    - cd module1 && npm test
    - cd module1 && npm run lint
deploy:
  module1/**:
  - cd module1 && npm run deploy
```

Then you can run tool from CI (`.gitlab-ci.yml`).
```sh
npm i gitlab-ci-selective
./node_modules/.bin/gitlab-ci-selective \
--token=$CI_JOB_TOKEN \
--project=$CI_PROJECT_ID \
--sha=$CI_COMMIT_SHA \
--stage=$CI_JOB_STAGE
```
