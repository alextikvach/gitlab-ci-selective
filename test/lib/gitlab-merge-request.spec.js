const sinon = require('sinon');
const expect = require('chai').expect;
const P = require('q');
const proxyquire = require('proxyquire');

const gitlabMrStub = sinon.stub();
const GitlabMergeRequest = proxyquire('../../lib/gitlab-merge-request', {
  'request-promise': gitlabMrStub,
});

describe('GitlabMergeRequest', () => {
  const sandbox = sinon.sandbox.create();

  afterEach(() => {
    sandbox.restore();
    gitlabMrStub.reset();
  });

  describe('constructor', () => {
    it('should throw if `token` arguments will not passed', () => {
      expect(() => new GitlabMergeRequest({}))
        .to.throw(/required arguments/);
    });
    it('should throw if `projectId` arguments will not passed', () => {
      expect(() => new GitlabMergeRequest({token: '1231'}))
        .to.throw(/required arguments/);
    });
    it('should throw if `sha` arguments will not passed', () => {
      expect(() => new GitlabMergeRequest({token: '1231', projectId: '12'}))
        .to.throw(/required arguments/);
    });
    it('should not throw if all required parameters was passed', () => {
      expect(() => new GitlabMergeRequest({token: '1231', projectId: '12', sha: 10}))
        .to.not.throw(/required arguments/);
    });
  });

  it('should use default endpoint api if it was not setted', () => {
    gitlabMrStub.returns(P.resolve([]));

    const mr = new GitlabMergeRequest({token: '1231', projectId: '12', sha: 10});

    return mr.affectedFiles()
      .catch(() => {
        expect(gitlabMrStub.firstCall.args[0].baseUrl)
          .to.equal('https://gitlab.com/api/v4/');
      });
  });

  it('should use passed endpoint api', () => {
    gitlabMrStub.returns(P.resolve([]));

    const mr = new GitlabMergeRequest({endpoint: 'kek.lol', token: '1231', projectId: '12', sha: 10});

    return mr.affectedFiles()
      .catch(() => {
        expect(gitlabMrStub.firstCall.args[0].baseUrl)
          .to.equal('kek.lol');
      });
  });

  it('should throw if there are no opened merge requests', () => {
    gitlabMrStub.returns(P.resolve([]));

    const mr = new GitlabMergeRequest({token: '1231', projectId: '12', sha: 10});

    expect(mr.affectedFiles())
      .to.eventually.rejectedWith('Unable to find open merge requests');
  });

  it('should throw if MR was not found by specifyed sha', () => {
    gitlabMrStub.returns(P.resolve([{sha: 20}]));

    const mr = new GitlabMergeRequest({token: '1231', projectId: '12', sha: 10});

    expect(mr.affectedFiles())
      .to.eventually.rejectedWith('Unable to find opened MR by sha=10');
  });

  it('should return affected files by `old_path` and `new_path`', () => {
    gitlabMrStub
      .onFirstCall()
        .returns(P.resolve([{sha: 10}]))
      .onSecondCall()
        .returns(P.resolve({changes: [{old_path: 'file1', new_path: 'file2'}]}));

    const mr = new GitlabMergeRequest({token: '1231', projectId: '12', sha: 10});

    expect(mr.affectedFiles())
      .to.become(['file1', 'file2']);
  });

  it('should return array of unique affected files', () => {
    gitlabMrStub
      .onFirstCall()
        .returns(P.resolve([{sha: 10, iid: 'needed-iid'}]))
      .onSecondCall()
        .returns(P.resolve({changes: [
          {old_path: 'file1', new_path: 'file1'},
          {old_path: 'file1', new_path: 'file2'},
          {old_path: 'file3', new_path: 'file2'},
        ]}));

    const mr = new GitlabMergeRequest({token: '1231', projectId: '12', sha: 10});

    expect(mr.affectedFiles())
      .to.become(['file1', 'file2', 'file3']);
  });
});
