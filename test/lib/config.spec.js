const sinon = require('sinon');
const expect = require('chai').expect;
const fs = require('fs');
const yaml = require('js-yaml');
const Config = require('../../lib/config');
const Stage = require('../../lib/stage');


describe('Config', () => {
  const sandbox = sinon.sandbox.create();

  afterEach(() => {
    sandbox.restore();
  });

  it('should load the YAML config by passed a path', () => {
    const configContent = 'key: val';

    sandbox.stub(fs, 'readFileSync').returns(configContent);
    const yamlLoaderSpy = sandbox.spy(yaml, 'safeLoad');

    const config = new Config(); // eslint-disable-line

    expect(yamlLoaderSpy).to.have.been.calledWithExactly(configContent);
  });

  it('should throw if the config has not loaded successfully', () => {
    sandbox.stub(fs, 'readFileSync').returns('23-12-3: [!');

    expect(() => {
      const config = new Config(); // eslint-disable-line
    }).to.throw(/Unable to load YAML file/);
  });

  it('should return a stage', () => {
    sandbox.stub(fs, 'readFileSync').returns([
      'stages:',
      '  some-stage: data',
    ].join('\n'));

    const config = new Config();

    expect(config.getStage('some-stage', [])).to.be.an.instanceOf(Stage);
  });

  it('should return the value of the option from settings', () => {
    sandbox.stub(fs, 'readFileSync').returns([
      'settings:',
      '  opt: val',
    ].join('\n'));

    const config = new Config();

    expect(config.getOption('opt')).to.eql('val');
  });

  it('should throw if there is not a stage in the config', () => {
    sandbox.stub(fs, 'readFileSync').returns([
      'stages:',
      '  testing: data',
    ].join('\n'));

    const config = new Config();

    expect(() => config.getStage('not-exists-stage', []))
      .to.throw(/There is no "not-exists-stage" stage in config file/);
  });
});
