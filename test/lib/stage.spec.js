const expect = require('chai').expect;
const Stage = require('../../lib/stage');

describe('Stage', () => {
  it('should return name of stage', () => {
    const stage = new Stage({stageName: 'some-stage'});

    expect(stage.name).to.be.equal('some-stage');
  });

  it('should return patterns only be affected file list', () => {
    const stage = new Stage({
      stageName: 'some-stage',
      declaration: {
        'project1/**': {
          commands: ['cmd1'],
          options: {
            cwd: 'path/1',
          },
        },
        'project2/**': {
          commands: ['cmd2'],
          options: {
            cwd: 'path/2',
          },
        },
      },
      affectedFileList: [
        'README.md',
        'project1/src/index.js',
        'project3/src/index.js',
      ],
    });

    expect(stage.patterns).to.be.eql([{
      pattern: 'project1/**',
      commands: ['cmd1'],
      options: {
        cwd: 'path/1',
      },
    }]);
  });

  it('should throw if if there are not commands for pattern', () => {
    const stage = new Stage({
      stageName: 'some-stage',
      declaration: {
        'project/**': {},
      },
      affectedFileList: [
        'project/src/index.js',
      ],
    });

    expect(() => stage.patterns).to.throw('There is no commands for "project/**" pattern');
  });
});
