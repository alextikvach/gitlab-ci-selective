const sinon = require('sinon');
const expect = require('chai').expect;
const cp = require('child_process');
const executor = require('../../lib/executor');
const logger = require('../../lib/logger');

describe('executor', () => {
  const sandbox = sinon.sandbox.create();
  let execStubData;

  beforeEach(() => {
    sandbox.stub(logger);

    execStubData = {
      stdout: {on: () => {}},
      stderr: {on: () => {}},
      on: (event, fn) => fn(0),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should execute commands sequntially', () => {
    sandbox.stub(cp, 'exec').returns(execStubData);

    const stage = {
      name: 'some-stage',
      patterns: [
        {pattern: 'proj1/**', commands: ['pwd', 'ls', 'time']},
        {pattern: 'proj2/**', commands: ['cd']},
      ],
    };

    return executor(stage).then(() => {
      expect(cp.exec.getCall(0)).to.have.been.calledWithExactly('pwd', sinon.match.object);
      expect(cp.exec.getCall(1)).to.have.been.calledWithExactly('ls', sinon.match.object);
      expect(cp.exec.getCall(2)).to.have.been.calledWithExactly('time', sinon.match.object);
      expect(cp.exec.getCall(3)).to.have.been.calledWithExactly('cd', sinon.match.object);
    });
  });

  it('should execute commands with passed cwd', () => {
    sandbox.stub(cp, 'exec').returns(execStubData);

    const stage = {
      name: 'some-stage',
      patterns: [
        {pattern: 'proj/**', commands: ['pwd'], options: {cwd: 'path/1'}},
      ],
    };

    return executor(stage).then(() => {
      expect(cp.exec.getCall(0)).to.have.been.calledWithExactly('pwd', sinon.match({
        cwd: 'path/1',
      }));
    });
  });

  it('should execute commands with passed env', () => {
    sandbox.stub(cp, 'exec').returns(execStubData);

    const stage = {
      name: 'some-stage',
      patterns: [
        {
          pattern: 'proj/**',
          commands: ['pwd'],
          options: {
            env: {
              /* eslint-disable */
              env1: '${USER}',
              env2: '${HOME}/project'},
              /* eslint-enable */
          },
        },
      ],
    };

    return executor(stage).then(() => {
      expect(cp.exec.getCall(0)).to.have.been.calledWithExactly('pwd', sinon.match({
        env: sinon.match({
          env1: process.env.USER,
          env2: `${process.env.HOME}/project`,
        }),
      }));
    });
  });

  it('should throw if there is no variable in env placeholder', () => {
    sandbox.stub(cp, 'exec').returns(execStubData);

    const stage = {
      name: 'some-stage',
      patterns: [
        {
          pattern: 'proj/**',
          commands: ['pwd'],
          options: {
            env: {
              /* eslint-disable */
              env1: '${NON_EXISTS}',
              env2: '${HOME}/project'},
              /* eslint-enable */
          },
        },
      ],
    };

    return expect(executor(stage)).to.eventually.be.rejectedWith(/Unable to resolve template/);
  });

  it('should throw if some of command has finished with non-zero code', () => {
    const execStub = sandbox.stub(cp, 'exec');

    execStub.returns(execStubData);
    execStub.onSecondCall().returns(Object.assign(execStubData, {
      on: (event, fn) => fn(234),
    }));

    const stage = {
      name: 'some-stage',
      patterns: [
        {pattern: 'proj1/**', commands: ['pwd', 'ls', 'time']},
        {pattern: 'proj2/**', commands: ['pwd']},
      ],
    };

    return expect(executor(stage)).to.eventually.rejectedWith(/finished with non-zero code - 234/);
  });
});
